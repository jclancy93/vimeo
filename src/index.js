import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import Channel from './components/channel';
import './index.css';

ReactDOM.render(
  <Channel url="http://vimeo.com/api/v2/channel/staffpicks/videos.json"pollInterval={160000}/>,
  document.getElementById('root')
);
