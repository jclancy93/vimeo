import React from 'react';

const Video = React.createClass({
  handleClick: function() {
  	window.location = this.props.data.url
  },
  render: function() {
    var videoStyle = {
      width: "100%",
      height: "100px",
      background: `url(${this.props.data.thumbnail_large}) center`,
      backgroundSize: "cover"
    };
    var userIconStyle = {
      borderRadius: "50%",
      height: "16px",
      width: "16px",
      display: "inline-block",
      background: `url(${this.props.data.user_portrait_small})`,
      backgroundSize: "cover"
    }
    return (
      <li className="video" data-id={this.props.data.id}>
        <div onClick={this.handleClick} style={videoStyle}
         className="video__preview" >
	      {/* 
	       	//Overlay markup
	      */}
          <i className="fa fa-play-circle-o fa-3x" aria-hidden="true"></i>
          <span>
            <i className="fa fa-clock-o" aria-hidden="true"></i>
            {this.props.data.duration_minutes}:{this.props.data.duration_seconds}
          </span>
        </div>
        {/* 
       	  //Video info markup
        */}
        <div className="video__info">
          <a href={this.props.data.url}><h2>{this.props.data.title}</h2></a>
          {/* 
       	    //Video info markup
          */}
          <div className="user-info">
            <span style={userIconStyle}></span>
            <span>
              <a href={this.props.data.user_url} className="user-link">
                {this.props.data.user_name}
              </a>
               | {this.props.data.stats_number_of_plays} plays
            </span>
          </div>
        </div>
      </li>
    );
  }
});

export default Video;