import React from 'react';

const VideoHeader = React.createClass({
  getInitialState: function() {
    return {
      data: [],
      index: 0,
    };
  },
  // Next button for carousel
  //returns to index 0 if exceeds array bounds
  handleClickNext: function() {
    if (this.state.index < this.props.data.length-1) {
      this.setState({
        index: this.state.index+1
      })
    }
    else {
      this.setState({index: 0})
      console.log('false')
    }
  },
  // Back button for carousel
  //returns to index (length-1) if exceeds array bounds
  handleClickPrev: function() {
    if (this.state.index > 0) {
      this.setState({
        data: this.props.data[this.state.index],
        index: this.state.index-1
      })
    }
    else {
      this.setState({index: 3})
      console.log('false')
    }
  },
  render: function() {
    var headerStyle = {
      width: "100%",
      height: "500px",
      background: `url(${this.props.data[this.state.index].thumbnail_large}) no-repeat center`
    };
    console.log(this.props, 'here are my props')
    return (
      <div style={headerStyle} className="header">
        <div className="header-info">
          {/* 
            //Overlay markup for video
            //Uses state index variable to iterate 
            //through props data array
          */}
          <h2>{this.props.data[this.state.index].title}</h2>
          <h2>by {this.props.data[this.state.index].user_name}</h2>
          <a href={this.props.data[this.state.index].url}>
            <button>
              <i className="fa fa-play" aria-hidden="true"></i>Watch
            </button> 
          </a>
        </div>
        {/* 
          //Carousel next/prev button markup
        */}
        <i className="fa fa-chevron-right fa-3x" aria-hidden="true"
           onClick={this.handleClickNext}></i>
        <i className="fa fa-chevron-left fa-3x" aria-hidden="true"
           onClick={this.handleClickPrev}></i>
      </div>
    );
  }
})

export default VideoHeader;