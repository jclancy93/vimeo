import React from 'react';
import Video from '../video/index.js'

const VideoList = React.createClass({
  render: function() {
    var videoNodes = this.props.data.map((video, index) => {
      // log video to check data 
      console.log(video, index)

      // do some data manip on video object 
      // formatting data and number of plays accordingly
      var seconds = video.duration
      var duration_minutes = Math.floor(seconds / 60)
      var duration_seconds = seconds - duration_minutes * 60;

      //left padding the time with a 0 if only 1 digit
      if (duration_minutes < 10) {
        video.duration_minutes = ('0' + duration_minutes).slice(-2)
      }
      else {
        video.duration_minutes = duration_minutes;
      }
      if (duration_seconds < 10) {
        console.log('left padding needed')
        video.duration_seconds = ('0' + duration_seconds).slice(-2)
      }
      else {
        video.duration_seconds = duration_seconds;
      }

      //  number of plays: 1000 => 1k (only if number > 999)
      if (video.stats_number_of_plays > 999) {
        video.stats_number_of_plays = (video.stats_number_of_plays /100).toFixed(1) + "K"
      }

      return (
        <Video data={video} key={index}>
        </Video>
      );
    });
    return (
      <ul className="video__list">
        {videoNodes}
      </ul>
    );
  }
});

export default VideoList;