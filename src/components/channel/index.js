import React from 'react';
import 'whatwg-fetch';
import VideoList from '../videoList/index.js'
import VideoHeader from '../videoHeader/index.js'


var Channel = React.createClass({
  getInitialState: function() {
    return {data: []};
  },
  loadCommentsFromServer: function() {
    //fecth api (returns promise)
    //then set state
    fetch(this.props.url).then(r => r.json())
      .then(data => this.setState({data:data}))
      .catch(e => console.log(e))
  },
  componentDidMount: function() {
    this.loadCommentsFromServer();
    //Set polling interval
    setInterval(this.loadCommentsFromServer, this.props.pollInterval);
  },
  render: function() {
    var value = 'stats_number_of_likes';
    return (
      <div className="main">
      <header>
        {this.state.data.length > 0 &&
          // only get first 4 video for the header component
          <VideoHeader data={this.state.data.slice(0,4)} />
        }
      </header>
      <main>
        <h2 className="section-title">
          {/*
            // Check for data, if none display loading... 
            // if data display most liked
          */}
          { this.state.data.length>0 ?  'Most Liked'  : 'Loading....'}
        </h2>
        {/*
          // Check for data then render component
          // sort data to show most liked videos first
        */}
        {this.state.data.length > 0 &&
        <VideoList 
        data={this.state.data.sort((a, b) => b[value] - a[value])}
        />
        }
      </main>
      </div>
    );
  }
});

export default Channel;